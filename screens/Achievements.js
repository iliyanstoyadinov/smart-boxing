import * as React from "react";
import {
  StyleSheet,
  Text,
  SafeAreaView,
  View,
  Platform,
  StatusBar,
  ScrollView,
} from "react-native";
import {
  Octicons,
  MaterialCommunityIcons,
  FontAwesome5,
} from "@expo/vector-icons";
import { ProgressBar } from "react-native-paper";

export default AchievementsScreen = ({ navigation }) => {
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.titleBox}>
          <Text style={styles.title}> Smart Boxing </Text>
        </View>
        <View style={styles.achievement}>
          <Octicons
            name="flame"
            size={68}
            color="#FF4D00"
            style={styles.achievementIcon}
          />
          <View style={styles.achivementView}>
            <ProgressBar
              progress={0.15}
              color="#FCFF00"
              style={styles.progressBar}
            />
            <Text style={styles.achText}>Reach 7 days {"\n"} streak</Text>
          </View>
        </View>
        <View style={styles.achievement}>
          <MaterialCommunityIcons
            name="boxing-glove"
            size={68}
            color="black"
            style={styles.achievementIcon}
          />
          <View style={styles.achivementView}>
            <ProgressBar
              progress={0}
              color="#FCFF00"
              style={styles.progressBar}
            />
            <Text style={styles.achText}>Complete Speed 1 {"\n"} Training</Text>
          </View>
        </View>
        <View style={styles.achievement}>
          <FontAwesome5
            name="user-friends"
            size={60}
            color="black"
            style={styles.achievementIcon}
          />
          <View style={styles.achivementView}>
            <ProgressBar
              progress={0.5}
              color="#FCFF00"
              style={styles.progressBar}
            />
            <Text style={styles.achText}>Invite two {"\n"} friend</Text>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#AAC7FF",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  achivementView: {
    justifyContent: "center",
    alignItems: "center",
    paddingLeft: 30,
    paddingVertical: 20,
  },
  achText: {
    textAlign: "center",
    fontSize: 14,
    paddingTop: 10,
  },
  achievementIcon: {
    paddingRight: 20,
  },
  achievement: {
    flexDirection: "row",
    marginTop: 40,
    justifyContent: "center",
    alignItems: "center",
    borderWidth: 1,
    borderRadius: 30,
    marginHorizontal: 30,
  },
  progressBar: {
    height: 15,
    width: 130,
    borderRadius: 20,
  },
  titleBox: {
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    borderBottomWidth: 15,
    borderBottomColor: "black",
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: 15,
  },
  title: {
    fontSize: 52,
    color: "black",
    fontFamily: "Marker",
  },
});
