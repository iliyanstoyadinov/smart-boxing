import React, { useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  TextInput,
  SafeAreaView,
} from "react-native";
import * as Facebook from "expo-facebook";
import * as Google from "expo-google-app-auth";
import { useFonts } from "expo-font";
import { useState } from "react";
import {
  getAuth,
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  onAuthStateChanged,
} from "firebase/auth";
import { initializeApp } from "firebase/app";
var tryit = true;

export default Login = ({ navigation }) => {
  const [loaded] = useFonts({
    Marker: require("../assets/fonts/PermanentMarker_400Regular.ttf"),
  });
  const firebaseConfig = {
    apiKey: "AIzaSyBM0LcjLEiCYYRU8PaC5Ojq0qOTSGUyVYw",
    authDomain: "smartboxing-auth.firebaseapp.com",
    databaseURL:
      "https://smartboxing-auth-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "smartboxing-auth",
    storageBucket: "smartboxing-auth.appspot.com",
    messagingSenderId: "629466828123",
    appId: "1:629466828123:web:f74f400be8080083839e7e",
    measurementId: "G-T6RMW0E3YX",
  };

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  useEffect(() => {
    const auth = getAuth();
    const unsubscribe = onAuthStateChanged(auth, (user) => {
      if (user) {
        navigation.navigate("MyStack");
      } else {
        console.log("Not signed in");
      }
      return unsubscribe;
    });
  }, []);

  const handleSignUp = () => {
    const auth = getAuth();
    createUserWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        // Signed in
        const user = userCredential.user;
        console.log("Registered with:", user.email);
        // ...
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        // ..
      });
  };
  const handleLogin = () => {
    const auth = getAuth();
    signInWithEmailAndPassword(auth, email, password)
      .then((userCredential) => {
        // Signed in
        const user = userCredential.user;
        console.log("Logged in with:", user.email);
        // ...
      })
      .catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;
      });
  };

  if (!loaded) {
    return null;
  }
  return (
    <SafeAreaView style={styles.container}>
      <View
        style={{
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Image
          style={{
            width: 200,
            height: 250,
          }}
          source={require("../assets/adaptive-icon.png")}
        />
        <Text style={styles.text}>Smart Boxing {"\n"} Login</Text>
      </View>
      <View style={styles.inputContainer}>
        <TextInput
          placeholder="Email Address: "
          value={email}
          onChangeText={(text) => setEmail(text)}
          style={styles.input}
        />

        <TextInput
          placeholder="Password: "
          value={password}
          onChangeText={(text) => setPassword(text)}
          style={styles.input}
          secureTextEntry
        />
      </View>
      <View style={{ flexDirection: "row", marginTop: 20 }}>
        <TouchableOpacity onPress={handleLogin} style={styles.buttonLogin}>
          <Text>Login</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={handleSignUp} style={styles.buttonRegister}>
          <Text>Register</Text>
        </TouchableOpacity>
      </View>
      <View
        style={{
          marginTop: 40,
          flexDirection: "row",
          justifyContent: "center",
        }}
      >
        <TouchableOpacity
          onPress={async () => {
            tryit = await FlogIn();
            if (tryit === 1) {
              navigation.navigate("MyStack");
            } else {
              alert("Login Cancelled!");
            }
          }}
        >
          <View style={styles.socialButtonF}>
            <Image
              source={require("../assets/facebook3x.png")}
              style={styles.socialLogo}
            />
            <Text style={styles.textButton}>Facebook</Text>
          </View>
        </TouchableOpacity>
      </View>
      <View
        style={{
          marginTop: 20,
          flexDirection: "row",
          justifyContent: "center",
        }}
      >
        <TouchableOpacity
          style={styles.socialButtonG}
          onPress={async () => {
            tryit = await signInWithGoogleAsync();
            if (tryit === 0) {
              alert("Login Cancelled!");
            } else {
              navigation.navigate("MyStack");
            }
          }}
        >
          <Image
            source={require("../assets/google3x.png")}
            style={styles.socialLogo}
          />
          <Text style={styles.textButton}>Google</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const accessToken = async (token) => {
  try {
    await AsyncStorage.setItem(token, GOOGLE_TOKEN);
  } catch (err) {
    throw new Error(err);
  }
};

async function signInWithGoogleAsync() {
  try {
    const { type, accessToken, user } = await Google.logInAsync({
      androidClientId:
        "1076287277894-5ci1a5mroe9k0nqc3vsrjr6csbtltfqr.apps.googleusercontent.com",
      iosClientId:
        "1076287277894-da78mnlso0l4l0njt4uat739pcnl5s4i.apps.googleusercontent.com",
      scopes: ["profile", "email"],
    });
    if (type === "success") {
      console.log(user);
      storeToken(result.accessToken);
      setAccessToken(results.accessToken);
    } else {
      return 0;
    }
  } catch (e) {
    return { error: true };
  }
}

async function FlogIn() {
  try {
    await Facebook.initializeAsync({
      appId: "831000140822692",
    });
    const { type, token, expirationDate, permissions, declinedPermissions } =
      await Facebook.logInWithReadPermissionsAsync({
        permissions: ["public_profile"],
      });
    if (type === "success") {
      // Get the user's name using Facebook's Graph API
      console.log("logged in to Facebook");
      const response = await fetch(
        `https://graph.facebook.com/me?access_token=${token}`
      );
      return 1;
    } else {
      // type === 'cancel'
    }
  } catch ({ message }) {
    alert(`Facebook Login Error: ${message}`);
  }
}

const styles = StyleSheet.create({
  buttonLogin: {
    width: 150,
    paddingVertical: 20,
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: "3%",
    borderRadius: 30,
    backgroundColor: "#FFF",
  },
  buttonRegister: {
    width: 150,
    paddingVertical: 20,
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: "3%",
    borderRadius: 30,
    backgroundColor: "#fff",
  },
  container: {
    flex: 1,
    backgroundColor: "#AAC7FF",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    alignItems: "center",
  },
  text: {
    color: "#1D2029",
    textAlign: "center",
    fontSize: 40,
    fontWeight: "500",
    fontFamily: "Marker",
  },
  textButton: {
    color: "#1D2029",
    textAlign: "center",
    marginTop: 15,
  },
  input: {
    backgroundColor: "white",
    paddingHorizontal: 15,
    paddingVertical: 10,
    borderRadius: 20,
    marginTop: 10,
  },
  inputContainer: {
    marginTop: 20,
    width: "85%",
  },
  socialButtonF: {
    flexDirection: "row",
    paddingVertical: 12,
    paddingHorizontal: 30,
    borderRadius: 50,
    backgroundColor: "#fff",
  },
  socialButtonG: {
    flexDirection: "row",
    paddingVertical: 12,
    paddingHorizontal: 37,
    borderRadius: 50,
    backgroundColor: "#fff",
  },
  socialLogo: {
    width: 48,
    height: 48,
    marginRight: 24,
  },
});
