import * as React from "react";
import {
  StyleSheet,
  Text,
  SafeAreaView,
  View,
  Pressable,
  Platform,
  StatusBar,
  ScrollView,
  Image,
} from "react-native";

export default DailyScreen = ({ navigation }) => {
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{ justifyContent: "center", alignItems: "center" }}>
          <Pressable
            style={styles.connectButton}
            onPress={() => {
              navigation.navigate({
                name: "Home",
              });
            }}
          >
            <Text style={styles.connectText}>Connect</Text>
          </Pressable>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#AAC7FF",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    paddingBottom: 20,
  },
  connectButton: {
    borderWidth: 1,
    borderRadius: 180,
    backgroundColor: "red",
    justifyContent: "center",
    alignItems: "center",
    width: 200,
    height: 100,
  },
  connectText: {
    fontSize: 32,
  },
});
