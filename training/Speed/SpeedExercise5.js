import * as React from "react";
import {
  StyleSheet,
  Text,
  SafeAreaView,
  View,
  TouchableOpacity,
  Platform,
  StatusBar,
  Image,
  ScrollView,
  useWindowDimensions,
} from "react-native";
import { ProgressBar } from "react-native-paper";
import { getDatabase, ref, onValue } from "firebase/database";
import { useState } from "react";
import { Timer } from "react-native-stopwatch-timer";
import { FontAwesome5 } from "@expo/vector-icons";
import { useFocusEffect } from "@react-navigation/native";

export default Exercise5 = ({ navigation }) => {
  useFocusEffect(
    React.useCallback(() => {
      let isActive = true;
      if (isActive) {
        setupPointsListener();
        console.log("Working screen 5");
      }
      return () => {
        isActive = false;
        console.log("Cleaned up screen 5");
      };
    }, [])
  );
  const screen = useWindowDimensions();
  const [points, setPoints] = useState(0);

  const [isTimerStart, setIsTimerStart] = useState(false);
  const [timerDuration, setTimerDuration] = useState(60000);
  const [resetTimer, setResetTimer] = useState(false);

  const setupPointsListener = () => {
    const db = getDatabase();
    const reference = ref(db, "adxl345/1-set");
    onValue(reference, (snapshot) => {
      var highscore = snapshot.val().Points;
      setPoints(highscore);
    });
    console.log("New high score: " + points);
  };

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.progress}>
          <ProgressBar
            progress={0.8}
            color="#FCFF00"
            style={[styles.progressBar, { width: screen.width / 1.1 }]}
          />
        </View>
        <View style={styles.pointsView}>
          <Text style={styles.pointsText}>Points: {points}</Text>
        </View>

        <View style={styles.imageExercise}>
          <Timer
            totalDuration={timerDuration}
            start={isTimerStart}
            reset={resetTimer}
            options={options}
            handleFinish={() => {
              alert("Set Done!");
              setResetTimer(true);
              setIsTimerStart(false);
            }}
          />
          <View style={{ flexDirection: "row" }}>
            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                setIsTimerStart(!isTimerStart);
                setResetTimer(false);
              }}
            >
              <Text style={styles.buttonText}>
                {!isTimerStart ? "START" : "STOP"}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                setIsTimerStart(false);
                setResetTimer(true);
              }}
            >
              <Text style={styles.buttonText}>RESET</Text>
            </TouchableOpacity>
          </View>

          <Image
            source={require("../TrainingAssets/Exercises/Speed1_5.png")}
            style={{ height: 400, width: 200 }}
          />
          <Text style={styles.mainText}>
            1 minute speed bag punches - 3 sets {"\n"} As fast as possible, keep
            arms up!
          </Text>
        </View>
        <View style={styles.viewContinue}>
          <TouchableOpacity
            style={styles.buttonContinue}
            onPress={() => navigation.navigate("Speed")}
          >
            <Text style={styles.textContinue}>Finish</Text>
            <FontAwesome5
              name="flag-checkered"
              size={32}
              color="black"
              style={{ padding: 5 }}
            />
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const options = {
  container: {
    backgroundColor: "rgba(0,0,0, 0.0)",
    width: 250,
    alignItems: "center",
  },
  text: {
    fontSize: 52,
    color: "black",
  },
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#AAC7FF",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    paddingBottom: 20,
  },
  button: {
    borderRadius: 40,
    borderWidth: 2,
    marginTop: 10,
    marginHorizontal: 20,
  },
  buttonText: {
    fontSize: 24,
    padding: 5,
  },
  buttonContinue: {
    borderWidth: 2,
    borderRadius: 40,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  viewContinue: {
    marginTop: 20,
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: 120,
  },
  textContinue: {
    fontSize: 32,
    padding: 10,
  },
  imageExercise: {
    marginTop: 5,
    justifyContent: "center",
    alignItems: "center",
  },
  mainText: {
    marginTop: 20,
    fontSize: 20,
    textAlign: "center",
  },
  pointsText: {
    fontSize: 52,
  },
  pointsView: {
    justifyContent: "center",
    alignItems: "center",
  },
  progress: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
  },
  progressBar: {
    height: 20,
    borderRadius: 20,
    marginBottom: 10,
  },
});
