import React from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  ScrollView,
  SafeAreaView,
} from "react-native";
import * as Facebook from "expo-facebook";
import * as Google from "expo-google-app-auth";
import Back from "../screens/Back";
import { getAuth, signOut } from "firebase/auth";

export default Logout = ({ navigation }) => {
  return (
    <SafeAreaView style={styles.container}>
      <View style={styles.row}>
        <Back navigation={navigation} />
        <Image
          style={styles.logoTitle}
          source={require("../assets/adaptive-icon.png")}
        />
      </View>
      <View
        style={{
          marginTop: 10,
          flexDirection: "row",
          justifyContent: "center",
        }}
      >
        <TouchableOpacity
          onPress={async () => {
            await handleSignOut();
            navigation.navigate("Login");
          }}
          style={styles.buttonSignOut}
        >
          <Text style={styles.text}>Sign Out</Text>
        </TouchableOpacity>
      </View>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "center",
        }}
      >
        <TouchableOpacity
          onPress={async () => {
            await FlogOut();
            navigation.navigate("Login");
          }}
        >
          <View style={styles.socialButtonF}>
            <Image
              source={require("../assets/facebook3x.png")}
              style={styles.socialLogo}
            />
            <Text style={styles.text}>Facebook logout</Text>
          </View>
        </TouchableOpacity>
      </View>
      <View
        style={{
          flexDirection: "row",
          justifyContent: "center",
        }}
      >
        <TouchableOpacity
          style={styles.socialButtonG}
          onPress={async () => {
            await logOutWithGoogleAsync();
            navigation.navigate("Login");
          }}
        >
          <Image
            source={require("../assets/google3x.png")}
            style={styles.socialLogo}
          />
          <Text style={styles.text}>Google logout</Text>
        </TouchableOpacity>
      </View>
    </SafeAreaView>
  );
};

const handleSignOut = () => {
  const auth = getAuth();
  signOut(auth)
    .then(() => {
      console.log("User signed out");
    })
    .catch((error) => {
      // An error happened.
    });
};

const accessToken = async (token) => {
  try {
    await AsyncStorage.setItem(token, GOOGLE_TOKEN);
  } catch (err) {
    throw new Error(err);
  }
};

async function logOutWithGoogleAsync() {
  await Google.logOutAsync({
    accessToken,
    androidClientId:
      "1076287277894-5ci1a5mroe9k0nqc3vsrjr6csbtltfqr.apps.googleusercontent.com",
    iosClientId:
      "1076287277894-da78mnlso0l4l0njt4uat739pcnl5s4i.apps.googleusercontent.com",
  });
  console.log("logged out of google");
}

async function FlogOut() {
  await Facebook.logOutAsync();
  console.log("logged out of facebook");
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#AAC7FF",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  text: {
    textAlign: "center",

    fontSize: 18,
  },
  row: {
    flexDirection: "row",
  },
  logoTitle: {
    width: 200,
    height: 250,
    right: "40%",
  },
  socialButtonF: {
    flexDirection: "row",
    paddingVertical: 12,
    paddingHorizontal: 30,
    borderRadius: 50,
    backgroundColor: "#fff",
    marginTop: 70,
    justifyContent: "center",
    alignItems: "center",
  },
  socialButtonG: {
    flexDirection: "row",
    paddingVertical: 12,
    paddingHorizontal: 38,
    borderRadius: 50,
    backgroundColor: "#fff",
    marginTop: 70,
    justifyContent: "center",
    alignItems: "center",
  },
  socialLogo: {
    width: 48,
    height: 48,
    marginRight: 24,
  },
  buttonSignOut: {
    paddingHorizontal: 90,
    paddingVertical: 22,
    justifyContent: "center",
    alignItems: "center",
    borderRadius: 50,
    backgroundColor: "#fff",
    marginTop: 40,
  },
});
