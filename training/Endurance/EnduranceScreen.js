import * as React from "react";
import {
  StyleSheet,
  Text,
  SafeAreaView,
  View,
  TouchableOpacity,
  Platform,
  StatusBar,
  ScrollView,
  Image,
  useWindowDimensions,
} from "react-native";

export default EnduranceScreen = ({ navigation }) => {
  const screen = useWindowDimensions();

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.titleBox}>
          <Text style={styles.title}> Smart Boxing </Text>
        </View>
        <View style={styles.endurance}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate({
                name: "Exercise1",
              });
            }}
            activeOpacity={0.7}
            style={styles.imageTitle}
          >
            <Image
              source={require("../TrainingAssets/Endurance1.png")} //created by master1305 - www.freepik.com
              style={[
                styles.image,
                { width: screen.width / 1.05, height: screen.height / 4.5 },
              ]}
            />
            <Text style={styles.text}>Endurance I</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.endurance}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate({
                name: "Exercise1",
              });
            }}
            activeOpacity={0.7}
            style={styles.imageTitle}
          >
            <Image
              source={require("../TrainingAssets/Endurance2.png")} //created by master1305 - www.freepik.com
              style={[
                styles.image,
                {
                  width: screen.width / 1.05,
                  height: screen.height / 4.5,
                },
              ]}
            />
            <Text style={styles.text}>Endurance II</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.endurance}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate({
                name: "Exercise1",
              });
            }}
            activeOpacity={0.7}
            style={styles.imageTitle}
          >
            <Image
              source={require("../TrainingAssets/Endurance3.png")} //created by master1305 - www.freepik.com
              style={[
                styles.image,
                { width: screen.width / 1.05, height: screen.height / 4.5 },
              ]}
            />
            <Text style={styles.text}>Endurance III</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.endurance}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate({
                name: "Exercise1",
              });
            }}
            activeOpacity={0.7}
            style={styles.imageTitle}
          >
            <Image
              source={require("../TrainingAssets/Endurance4.png")} //created by pvproductions - www.freepik.com
              style={[
                styles.image,
                { width: screen.width / 1.05, height: screen.height / 4.5 },
              ]}
            />
            <Text style={styles.text}>Endurance IV</Text>
          </TouchableOpacity>
        </View>
        <View style={[styles.endurance, { marginBottom: screen.height / 6 }]}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate({
                name: "Exercise1",
              });
            }}
            activeOpacity={0.7}
            style={styles.imageTitle}
          >
            <Image
              source={require("../TrainingAssets/Endurance.png")} //created by master1305 - www.freepik.com
              style={[
                styles.image,
                { width: screen.width / 1.05, height: screen.height / 4.5 },
              ]}
            />
            <Text style={styles.text}>Endurance V</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#AAC7FF",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  image: {
    borderRadius: 50,
  },
  imageTitle: {
    justifyContent: "center",
    alignItems: "center",
  },
  endurance: {
    marginTop: 40,
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    position: "absolute",
    fontFamily: "Marker",
    fontSize: 40,
    color: "#FCFF00",
  },
  titleBox: {
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    borderBottomWidth: 15,
    borderBottomColor: "black",
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: 15,
  },
  title: {
    fontSize: 52,
    color: "black",
    fontFamily: "Marker",
  },
});
