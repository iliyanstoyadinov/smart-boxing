import * as React from "react";
import {
  StyleSheet,
  Text,
  SafeAreaView,
  View,
  TouchableOpacity,
  Platform,
  StatusBar,
  ScrollView,
  Image,
  useWindowDimensions,
} from "react-native";

export default PowerScreen = ({ navigation }) => {
  const screen = useWindowDimensions();

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.titleBox}>
          <Text style={styles.title}> Smart Boxing </Text>
        </View>
        <View style={styles.power}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate({
                name: "Exercise1",
              });
            }}
            activeOpacity={0.7}
            style={styles.imageTitle}
          >
            <Image
              source={require("../TrainingAssets/Power3.png")} //created by master1305 - www.freepik.com
              style={[
                styles.image,
                { width: screen.width / 1.05, height: screen.height / 4.5 },
              ]}
            />
            <Text style={styles.text}>Power I</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.power}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate({
                name: "Exercise1",
              });
            }}
            activeOpacity={0.7}
            style={styles.imageTitle}
          >
            <Image
              source={require("../TrainingAssets/Power2.png")} //created by master1305 - www.freepik.com
              style={[
                styles.image,
                {
                  width: screen.width / 1.05,
                  height: screen.height / 4.5,
                },
              ]}
            />
            <Text style={styles.text}>Power II</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.power}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate({
                name: "Exercise1",
              });
            }}
            activeOpacity={0.7}
            style={styles.imageTitle}
          >
            <Image
              source={require("../TrainingAssets/Power1.png")} //created by master1305 - www.freepik.com
              style={[
                styles.image,
                { width: screen.width / 1.05, height: screen.height / 4.5 },
              ]}
            />
            <Text style={styles.text}>Power III</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.power}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate({
                name: "Exercise1",
              });
            }}
            activeOpacity={0.7}
            style={styles.imageTitle}
          >
            <Image
              source={require("../TrainingAssets/Power4.png")} //created by serhii_bobyk - www.freepik.com
              style={[
                styles.image,
                { width: screen.width / 1.05, height: screen.height / 4.5 },
              ]}
            />
            <Text style={styles.text}>Power IV</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.power}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate({
                name: "Exercise1",
              });
            }}
            activeOpacity={0.7}
            style={styles.imageTitle}
          >
            <Image
              source={require("../TrainingAssets/Power.png")} //created by master1305 - www.freepik.com
              style={[
                styles.image,
                { width: screen.width / 1.05, height: screen.height / 4.5 },
              ]}
            />
            <Text style={styles.text}>Power V</Text>
          </TouchableOpacity>
        </View>
        <View style={[styles.power, { marginBottom: screen.height / 6 }]}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate({
                name: "Exercise1",
              });
            }}
            activeOpacity={0.7}
            style={styles.imageTitle}
          >
            <Image
              source={require("../TrainingAssets/Power5.png")} //created by gpointstudio - www.freepik.com
              style={[
                styles.image,
                { width: screen.width / 1.05, height: screen.height / 4.5 },
              ]}
            />
            <Text style={styles.text}>Power VI</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#AAC7FF",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  image: {
    borderRadius: 50,
  },
  imageTitle: {
    justifyContent: "center",
    alignItems: "center",
  },
  power: {
    marginTop: 40,
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    position: "absolute",
    fontFamily: "Marker",
    fontSize: 40,
    color: "#FCFF00",
  },
  titleBox: {
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    borderBottomWidth: 15,
    borderBottomColor: "black",
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: 15,
  },
  title: {
    fontSize: 52,
    color: "black",
    fontFamily: "Marker",
  },
});
