import * as React from "react";
import {
  StyleSheet,
  Text,
  SafeAreaView,
  View,
  TouchableOpacity,
  Platform,
  StatusBar,
  Image,
  useWindowDimensions,
} from "react-native";
import { ProgressBar } from "react-native-paper";
import { useState } from "react";
import { Timer } from "react-native-stopwatch-timer";
import { Entypo } from "@expo/vector-icons";

export default Exercise1 = ({ navigation }) => {
  const screen = useWindowDimensions();

  const [isTimerStart, setIsTimerStart] = useState(false);
  const [timerDuration, setTimerDuration] = useState(60000);
  const [resetTimer, setResetTimer] = useState(false);

  return (
    <SafeAreaView style={styles.container}>
      <View>
        <View style={styles.progress}>
          <ProgressBar
            progress={0}
            color="#FCFF00"
            style={[styles.progressBar, { width: screen.width / 1.1 }]}
          />
        </View>
        <View style={styles.imageExercise}>
          <Timer
            totalDuration={timerDuration}
            start={isTimerStart}
            reset={resetTimer}
            options={options}
            handleFinish={() => {
              alert("Set Done!");
              setResetTimer(true);
              setIsTimerStart(false);
            }}
          />
          <View style={{ flexDirection: "row" }}>
            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                setIsTimerStart(!isTimerStart);
                setResetTimer(false);
              }}
            >
              <Text style={styles.buttonText}>
                {!isTimerStart ? "START" : "STOP"}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                setIsTimerStart(false);
                setResetTimer(true);
              }}
            >
              <Text style={styles.buttonText}>RESET</Text>
            </TouchableOpacity>
          </View>
          <Image
            source={require("../TrainingAssets/Exercises/Speed1_1.png")}
            style={{ height: 400, width: screen.width }}
          />
          <Text style={styles.mainText}>
            1 minute jump rope - 3 sets {"\n"} Alternatively hop on spot
          </Text>
        </View>
        <View style={styles.viewContinue}>
          <TouchableOpacity
            style={styles.buttonContinue}
            onPress={() => navigation.navigate("SpeedExercise2")}
          >
            <Text style={styles.textContinue}>Continue</Text>
            <Entypo name="arrow-bold-right" size={40} color="black" />
          </TouchableOpacity>
        </View>
      </View>
    </SafeAreaView>
  );
};

const options = {
  container: {
    backgroundColor: "rgba(0,0,0, 0.0)",
    width: 250,
    alignItems: "center",
  },
  text: {
    fontSize: 52,
    color: "black",
  },
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#AAC7FF",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    paddingBottom: 20,
  },
  button: {
    borderRadius: 40,
    borderWidth: 2,
    marginTop: 10,
    marginHorizontal: 20,
  },
  buttonText: {
    fontSize: 24,
    padding: 5,
  },
  buttonContinue: {
    borderWidth: 2,
    borderRadius: 40,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  viewContinue: {
    marginTop: 20,
    justifyContent: "center",
    alignItems: "center",
  },
  textContinue: {
    fontSize: 32,
    padding: 10,
  },
  imageExercise: {
    marginTop: 5,
    justifyContent: "center",
    alignItems: "center",
  },
  mainText: {
    marginTop: 20,
    fontSize: 20,
    textAlign: "center",
  },
  progress: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 20,
  },
  progressBar: {
    height: 20,
    borderRadius: 20,
    marginBottom: 10,
  },
});
