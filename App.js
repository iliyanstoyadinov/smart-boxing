import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import MyTabs from "./screens/Stack";
import ConnectScreen from "./screens/ConnectScreen";
import WeeklyScreen from "./screens/WeeklyScreen";
import Login from "./screens/Login";
import Logout from "./screens/Logout";
import DailyScreen from "./screens/DailyScreen";

const Stack = createStackNavigator();

const MyStack = () => (
  <Stack.Navigator screenOptions={{ headerShown: false }}>
    <Stack.Screen name="HomeTAB" component={MyTabs} />
    <Stack.Screen
      name="Connect"
      component={ConnectScreen}
      options={{ gestureEnabled: false }}
    />
    <Stack.Screen
      name="Daily"
      component={DailyScreen}
      //options={{ gestureEnabled: false }}
    />
    <Stack.Screen
      name="Weekly"
      component={WeeklyScreen}
      //options={{ gestureEnabled: false }}
    />
    <Stack.Screen name="Logout" component={Logout} />
  </Stack.Navigator>
);

const CombinedStack = createStackNavigator();

const MyCombinedStack = () => (
  <CombinedStack.Navigator
    screenOptions={{ headerShown: false, gestureEnabled: false }}
  >
    <CombinedStack.Screen name="Login" component={Login} />
    <CombinedStack.Screen name="MyStack" component={MyStack} />
  </CombinedStack.Navigator>
);

export default function App() {
  return (
    <NavigationContainer>
      <MyCombinedStack />
    </NavigationContainer>
  );
}
