import * as React from "react";
import {
  StyleSheet,
  Text,
  SafeAreaView,
  View,
  TouchableOpacity,
  Platform,
  StatusBar,
  ScrollView,
  Image,
  useWindowDimensions,
} from "react-native";

export default SpeedScreen = ({ navigation }) => {
  const screen = useWindowDimensions();

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.titleBox}>
          <Text style={styles.title}> Smart Boxing </Text>
        </View>
        <View style={styles.speed}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate({
                name: "SpeedExercise1",
              });
            }}
            activeOpacity={0.7}
            style={styles.imageTitle}
          >
            <Image
              source={require("../TrainingAssets/Speed2.png")} //created by master1305 - www.freepik.com
              style={[
                styles.image,
                { width: screen.width / 1.05, height: screen.height / 4.5 },
              ]}
            />
            <Text style={styles.text}>Speed I</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.speed}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate({
                name: "Exercise1",
              });
            }}
            activeOpacity={0.7}
            style={styles.imageTitle}
          >
            <Image
              source={require("../TrainingAssets/Speed4.png")} //created by master1305 - www.freepik.com
              style={[
                styles.image,
                {
                  width: screen.width / 1.05,
                  height: screen.height / 4.5,
                },
              ]}
            />
            <Text style={styles.text}>Speed II</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.speed}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate({
                name: "Exercise1",
              });
            }}
            activeOpacity={0.7}
            style={styles.imageTitle}
          >
            <Image
              source={require("../TrainingAssets/Speed.png")} //created by master1305 - www.freepik.com
              style={[
                styles.image,
                { width: screen.width / 1.05, height: screen.height / 4.5 },
              ]}
            />
            <Text style={styles.text}>Speed III</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.speed}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate({
                name: "Exercise1",
              });
            }}
            activeOpacity={0.7}
            style={styles.imageTitle}
          >
            <Image
              source={require("../TrainingAssets/Speed1.png")} //created by serhii_bobyk - www.freepik.com
              style={[
                styles.image,
                { width: screen.width / 1.05, height: screen.height / 4.5 },
              ]}
            />
            <Text style={styles.text}>Speed IV</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.speed}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate({
                name: "Exercise1",
              });
            }}
            activeOpacity={0.7}
            style={styles.imageTitle}
          >
            <Image
              source={require("../TrainingAssets/Speed3.png")} //created by master1305 - www.freepik.com
              style={[
                styles.image,
                { width: screen.width / 1.05, height: screen.height / 4.5 },
              ]}
            />
            <Text style={styles.text}>Speed V</Text>
          </TouchableOpacity>
        </View>
        <View style={[styles.speed, { marginBottom: screen.height / 6 }]}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate({
                name: "Exercise1",
              });
            }}
            activeOpacity={0.7}
            style={styles.imageTitle}
          >
            <Image
              source={require("../TrainingAssets/Speed1.png")} //created by master1305 - www.freepik.com
              style={[
                styles.image,
                { width: screen.width / 1.05, height: screen.height / 4.5 },
              ]}
            />
            <Text style={styles.text}>Speed VI</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#AAC7FF",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  image: {
    borderRadius: 50,
  },
  imageTitle: {
    justifyContent: "center",
    alignItems: "center",
  },
  speed: {
    marginTop: 40,
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    position: "absolute",
    fontFamily: "Marker",
    fontSize: 40,
    color: "#FCFF00",
  },
  titleBox: {
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    borderBottomWidth: 15,
    borderBottomColor: "black",
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: 15,
  },
  title: {
    fontSize: 52,
    color: "black",
    fontFamily: "Marker",
  },
});
