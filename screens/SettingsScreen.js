import * as React from "react";
import {
  StyleSheet,
  TouchableOpacity,
  Text,
  SafeAreaView,
  View,
  ScrollView,
  Platform,
  StatusBar,
} from "react-native";
import { MaterialIcons } from "@expo/vector-icons";

export default SettingsScreen = ({ navigation }) => {
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.titleBox}>
          <Text style={styles.title}> Smart Boxing </Text>
        </View>
        <View style={styles.logout}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate({
                name: "Logout",
              });
            }}
            style={styles.logoutButton}
          >
            <Text style={styles.logoutText}>Logout</Text>
            <MaterialIcons
              name="logout"
              size={24}
              color="black"
              style={styles.logoutIcon}
            />
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#AAC7FF",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  logout: {
    justifyContent: "center",
    alignItems: "center",
    marginTop: 30,
  },
  logoutButton: {
    flexDirection: "row",
    borderWidth: 1,
    borderRadius: 30,
    paddingHorizontal: 40,
  },
  logoutIcon: {
    paddingVertical: 24,
  },
  logoutText: {
    fontSize: 26,
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  titleBox: {
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    borderBottomWidth: 15,
    borderBottomColor: "black",
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: 15,
  },
  title: {
    fontSize: 52,
    color: "black",
    fontFamily: "Marker",
  },
});
