import * as React from "react";
import {
  StyleSheet,
  Text,
  SafeAreaView,
  View,
  Pressable,
  Platform,
  StatusBar,
  ImageBackground,
  ScrollView,
} from "react-native";
import { useState, useEffect } from "react";
import { MaterialCommunityIcons } from "@expo/vector-icons";
import moment from "moment";
import HorizontalScroll from "./NewsList";

export default HomeScreen = ({ navigation, route }) => {
  const mainUrl =
    "https://newsapi.org/v2/top-headlines?q=sport&language=en&apiKey=deaaf32cc75c439b9803543754a7aee3";

  const [streak, setStreak] = useState(0);
  const [played, setPlayed] = useState(0);
  const [lastPlayed, setLastPlayed] = useState("");
  const [currentDate, setCurrentDate] = useState("");

  useEffect(() => {
    var date = moment().utcOffset("+2").format("ddd");
    setCurrentDate(date);
  }, []);

  const streakCount = () => {
    if (route.params?.state === true) {
      if (lastPlayed !== currentDate) {
        setPlayed(0);
      }
      if (played === 0) {
        setStreak(streak + 1);
        setPlayed(1);
        var last = moment().utcOffset("+2").format("ddd");
        setLastPlayed(last);
      }
    }
  };

  function handleClick() {
    if (route.params?.state === true) {
      navigation.navigate("SpeedExercise1");
    } else {
      alert("Please Connect a Device");
    }
  }

  function weeklyChallenge() {
    if (route.params?.state === true) {
      if (currentDate === "Sun") {
        navigation.navigate("Weekly");
      } else {
        alert("Available Sunday!");
      }
    } else {
      alert("Please Connect a Device");
    }
  }

  function combinedFunction() {
    handleClick();
    streakCount();
  }
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.row}>
          <Text style={styles.title}> Smart Boxing </Text>
        </View>
        <View style={styles.streak}>
          <MaterialCommunityIcons
            style={{ marginHorizontal: 5 }}
            name="fire"
            size={50}
            color={streak === 0 ? "#4F4F4F" : "#FF4D00"}
          />
          <Text style={styles.streakText}>{streak} Days</Text>
        </View>
        <ImageBackground
          style={styles.background}
          imageStyle={{ opacity: route.params?.state === true ? 1 : 0.6 }}
          source={require("../assets/HousingRender.png")}
        >
          <View>
            <Pressable
              style={{
                position: "absolute",
                backgroundColor: "rgba(0,0,0, 0.0)",
                width: 220,
                height: 80,
                marginTop: 140,
                marginHorizontal: 6,
                borderRadius: 180,
                borderWidth: 2,
                borderColor: "black",
                justifyContent: "center",
                alignSelf: "center",
                alignItems: "center",
                display: route.params?.state === true ? "none" : "flex",
              }}
              onPress={() => navigation.navigate("Connect")}
            >
              <Text style={styles.connectText}>Connect Device</Text>
            </Pressable>
            <Text
              style={{
                justifyContent: "center",
                padding: 10,
                fontSize: 30,
                marginTop: 130,
                color: "#FCFF00",
                fontFamily: "Marker",
                display: route.params?.state === undefined ? "none" : "flex",
              }}
            >
              Device: TR8BT6
            </Text>
          </View>
        </ImageBackground>
        <View style={styles.bubblesContainer}>
          <Pressable style={styles.dailyButton} onPress={combinedFunction}>
            <Text style={styles.dailyText}>
              Daily{"\n"}
              Training
            </Text>
          </Pressable>
          <Pressable
            style={{
              flexDirection: "row",
              backgroundColor:
                currentDate === "Sun" ? "#FCFF00" : "rgba(252, 255, 0, 0.3 )",
              width: 160,
              height: 160,
              borderRadius: 180,
              justifyContent: "center",
              alignItems: "center",
              marginHorizontal: "5.5%",
            }}
            onPress={weeklyChallenge}
          >
            <Text
              style={[
                styles.weeklyText,
                { opacity: currentDate === "Sun" ? 1 : 0.6 },
              ]}
            >
              Weekly Challenge
            </Text>
          </Pressable>
        </View>
        <View style={styles.news}>
          <Text style={styles.newsText}>Latest Sport News</Text>
        </View>
        <View style={styles.newsContainer}>
          <HorizontalScroll url={mainUrl} />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  background: {
    flex: 1,
    width: "100%",
    height: 400,
    alignItems: "center",
  },
  bubblesContainer: {
    flexDirection: "row",
  },
  connectText: {
    fontSize: 26,
    fontFamily: "Marker",
    color: "#FCFF00",
  },
  container: {
    flex: 1,
    backgroundColor: "#AAC7FF",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
    paddingBottom: 20,
  },
  dailyText: {
    fontFamily: "Marker",
    fontSize: 22,
    textAlign: "center",
  },
  dailyButton: {
    flexDirection: "row",
    backgroundColor: "#FCFF00",
    width: 160,
    height: 160,
    borderRadius: 180,
    justifyContent: "center",
    alignItems: "center",
    marginHorizontal: "5.5%",
  },
  row: {
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    borderBottomWidth: 15,
    borderBottomColor: "black",
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: 15,
  },
  streak: {
    flexDirection: "row",
    justifyContent: "center",
  },
  streakText: {
    alignSelf: "flex-end",
    fontSize: 32,
    fontFamily: "Marker",
  },
  title: {
    fontSize: 52,
    color: "black",
    fontFamily: "Marker",
  },
  news: {
    paddingTop: 40,
    paddingBottom: 15,
    justifyContent: "center",
    alignItems: "center",
  },
  newsText: {
    fontFamily: "Marker",
    fontSize: 38,
    paddingBottom: 20,
  },
  newsContainer: {
    paddingBottom: 120,
  },
  weeklyText: {
    fontFamily: "Marker",
    fontSize: 22,
    textAlign: "center",
  },
});
