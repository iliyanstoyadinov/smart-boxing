import * as React from "react";
import {
  StyleSheet,
  Text,
  SafeAreaView,
  View,
  TouchableOpacity,
  Platform,
  StatusBar,
  ScrollView,
  Image,
  useWindowDimensions,
} from "react-native";

export default TechniqueScreen = ({ navigation }) => {
  const screen = useWindowDimensions();

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.titleBox}>
          <Text style={styles.title}> Smart Boxing </Text>
        </View>
        <View style={styles.technique}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate({
                name: "Exercise1",
              });
            }}
            activeOpacity={0.7}
            style={styles.imageTitle}
          >
            <Image
              source={require("../TrainingAssets/Technique1.png")} //created by freepik - www.freepik.com</a>
              style={[
                styles.image,
                { width: screen.width / 1.05, height: screen.height / 4.5 },
              ]}
            />
            <Text style={styles.text}>Technique I</Text>
          </TouchableOpacity>
        </View>
        <View style={[styles.technique, { marginBottom: screen.height / 6 }]}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate({
                name: "Exercise1",
              });
            }}
            activeOpacity={0.7}
            style={styles.imageTitle}
          >
            <Image
              source={require("../TrainingAssets/Technique.png")} //created by master1305 - www.freepik.com
              style={[
                styles.image,
                { width: screen.width / 1.05, height: screen.height / 4.5 },
              ]}
            />
            <Text style={styles.text}>Technique II</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#AAC7FF",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  image: {
    borderRadius: 50,
  },
  imageTitle: {
    justifyContent: "center",
    alignItems: "center",
  },
  technique: {
    marginTop: 40,
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    position: "absolute",
    fontFamily: "Marker",
    fontSize: 40,
    color: "#FCFF00",
  },
  titleBox: {
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    borderBottomWidth: 15,
    borderBottomColor: "black",
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: 15,
  },
  title: {
    fontSize: 52,
    color: "black",
    fontFamily: "Marker",
  },
});
