import * as React from "react";
import {
  StyleSheet,
  Text,
  SafeAreaView,
  View,
  Platform,
  StatusBar,
  ScrollView,
} from "react-native";
import { TouchableOpacity } from "react-native-gesture-handler";
import { FontAwesome5 } from "@expo/vector-icons";

export default LeaderboardScreen = ({ navigation }) => {
  return (
    <SafeAreaView style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.titleBox}>
          <Text style={styles.title}> Smart Boxing </Text>
        </View>
        <View style={styles.table}>
          <View style={styles.rows}>
            <Text style={styles.rank}>1</Text>
            <FontAwesome5
              name="medal"
              size={24}
              color="gold"
              style={styles.medal}
            />
            <Text style={styles.name}>Iliyan Stoyadinov</Text>
            <Text style={styles.points}>2573</Text>
          </View>
          <View style={styles.rows}>
            <Text style={styles.rank}>2</Text>
            <FontAwesome5
              name="medal"
              size={24}
              color="#DEDEDE"
              style={styles.medal}
            />
            <Text style={styles.name}>Test 1</Text>
            <Text style={styles.points}>1865</Text>
          </View>
          <View style={styles.rows}>
            <Text style={styles.rank}>3</Text>
            <FontAwesome5
              name="medal"
              size={24}
              color="#CD7F32"
              style={styles.medal}
            />
            <Text style={styles.name}>Test 2</Text>
            <Text style={styles.points}>1765</Text>
          </View>
          <View style={styles.rows}>
            <Text style={styles.rank}>4</Text>
            <Text style={styles.empty}></Text>
            <Text style={styles.name}>Test 3</Text>
            <Text style={styles.points}>1763</Text>
          </View>
          <View style={styles.rows}>
            <Text style={styles.rank}>5</Text>
            <Text style={styles.empty}></Text>
            <Text style={styles.name}>Test 4</Text>
            <Text style={styles.points}>1658</Text>
          </View>
          <View style={styles.rows}>
            <Text style={styles.rank}>6</Text>
            <Text style={styles.empty}></Text>
            <Text style={styles.name}>Test 5</Text>
            <Text style={styles.points}>1523</Text>
          </View>
          <View style={styles.rows}>
            <Text style={styles.rank}>7</Text>
            <Text style={styles.empty}></Text>
            <Text style={styles.name}>Test 6</Text>
            <Text style={styles.points}>1325</Text>
          </View>
        </View>
        <View style={styles.invite}>
          <TouchableOpacity>
            <Text style={styles.inviteText}> Invite Friends </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#AAC7FF",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  empty: {
    width: 24,
  },
  invite: {
    justifyContent: "center",
    alignItems: "center",
  },
  inviteText: {
    fontSize: 22,
    marginTop: 20,
    color: "blue",
  },
  medal: {
    paddingVertical: 20,
  },
  rows: {
    marginBottom: 10,
    flexDirection: "row",
    borderWidth: 1,
    borderRadius: 30,
  },
  rank: {
    width: 50,
    paddingVertical: 20,
    paddingLeft: 20,
    fontSize: 22,
  },
  name: {
    width: 180,
    paddingLeft: 10,
    paddingVertical: 20,
    fontSize: 22,
  },
  points: {
    width: 100,
    textAlign: "center",
    paddingVertical: 20,
    fontSize: 22,
  },
  table: {
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
  },
  titleBox: {
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    borderBottomWidth: 15,
    borderBottomColor: "black",
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: 15,
  },
  title: {
    fontSize: 52,
    color: "black",
    fontFamily: "Marker",
  },
});
