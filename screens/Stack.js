import * as React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import {
  MaterialIcons,
  MaterialCommunityIcons,
  FontAwesome5,
  Ionicons,
} from "@expo/vector-icons";
import HomeScreen from "./HomeScreen";
import SettingsScreen from "./SettingsScreen";
import AchievementsScreen from "./Achievements";
import LeaderboardScreen from "./Leaderboard";
import TrainingStack from "../training/TrainingStack";
import { useWindowDimensions } from "react-native";

const Tabs = createBottomTabNavigator();

export default MyTabs = ({ navigation }) => {
  const screen = useWindowDimensions();
  return (
    <Tabs.Navigator
      screenOptions={{
        tabBarInactiveTintColor: "black",
        tabBarActiveTintColor: "#FCFF00",
        headerShown: false,
        tabBarLabelStyle: { fontSize: 12 },
        tabBarStyle: {
          position: "absolute",
          borderTopColor: "#395CAC",
          backgroundColor: "#395CAC",
          borderRadius: 40,
          height: screen.height / 9,
        },
      }}
    >
      <Tabs.Screen
        name="Home"
        component={HomeScreen}
        options={{
          tabBarIcon: ({ focused }) => (
            <FontAwesome5
              name="home"
              size={focused ? 40 : 30}
              color={focused ? "#FCFF00" : "black"}
            />
          ),
        }}
      />

      <Tabs.Screen
        name="Leaderboard"
        component={LeaderboardScreen}
        options={{
          tabBarIcon: ({ focused }) => (
            <MaterialIcons
              name="leaderboard"
              size={focused ? 44 : 34}
              color={focused ? "#FCFF00" : "black"}
            />
          ),
        }}
      />
      <Tabs.Screen
        name="Training"
        component={TrainingStack}
        options={{
          tabBarIcon: ({ focused }) => (
            <MaterialCommunityIcons
              name="boxing-glove"
              size={focused ? 46 : 36}
              color={focused ? "#FCFF00" : "black"}
            />
          ),
        }}
      />
      <Tabs.Screen
        name="Achievements"
        component={AchievementsScreen}
        options={{
          tabBarIcon: ({ focused }) => (
            <FontAwesome5
              name="trophy"
              size={focused ? 40 : 30}
              color={focused ? "#FCFF00" : "black"}
            />
          ),
        }}
      />
      <Tabs.Screen
        name="Settings"
        component={SettingsScreen}
        options={{
          tabBarIcon: ({ focused }) => (
            <Ionicons
              name="settings"
              size={focused ? 42 : 32}
              color={focused ? "#FCFF00" : "black"}
            />
          ),
        }}
      />
    </Tabs.Navigator>
  );
};
