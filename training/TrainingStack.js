import * as React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import SpeedExercise1 from "./Speed/SpeedExercise1";
import SpeedExercise2 from "./Speed/SpeedExercise2";
import SpeedExercise3 from "./Speed/SpeedExercise3";
import SpeedExercise4 from "./Speed/SpeedExercise4";
import SpeedExercise5 from "./Speed/SpeedExercise5";
import TrainingScreen from "./TrainingScreen";
import SpeedScreen from "./Speed/SpeedScreen";
import EnduranceScreen from "./Endurance/EnduranceScreen";
import PowerScreen from "./Power/PowerScreen";
import AgilityScreen from "./Agility/AgilityScreen";
import TechniqueScreen from "./Technique/TechniqueScreen";
import AbsScreen from "./Abs/AbsScreen";

const DayStack = createStackNavigator();

export default TrainingStack = ({ navigation }) => {
  return (
    <DayStack.Navigator screenOptions={{ headerShown: false }}>
      <DayStack.Screen
        name="Training"
        component={TrainingScreen}
        options={{ gestureEnabled: false }}
      />
      <DayStack.Screen name="Speed" component={SpeedScreen} />
      <DayStack.Screen name="Endurance" component={EnduranceScreen} />
      <DayStack.Screen name="Power" component={PowerScreen} />
      <DayStack.Screen name="Agility" component={AgilityScreen} />
      <DayStack.Screen name="Technique" component={TechniqueScreen} />
      <DayStack.Screen name="Abs" component={AbsScreen} />
      <DayStack.Screen
        name="SpeedExercise1"
        component={SpeedExercise1}
        options={{ gestureEnabled: false }}
      />
      <DayStack.Screen
        name="SpeedExercise2"
        component={SpeedExercise2}
        options={{ gestureEnabled: false }}
      />
      <DayStack.Screen
        name="SpeedExercise3"
        component={SpeedExercise3}
        options={{ gestureEnabled: false }}
      />
      <DayStack.Screen
        name="SpeedExercise4"
        component={SpeedExercise4}
        options={{ gestureEnabled: false }}
      />
      <DayStack.Screen
        name="SpeedExercise5"
        component={SpeedExercise5}
        options={{ gestureEnabled: false }}
      />
    </DayStack.Navigator>
  );
};
