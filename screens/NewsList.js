import * as React from "react";
import {
  StyleSheet,
  View,
  Text,
  ImageBackground,
  ActivityIndicator,
  TouchableOpacity,
  Linking,
} from "react-native";
import { FlatList } from "react-native-gesture-handler";
import { useState, useEffect } from "react";
import moment from "moment";
import { LinearGradient } from "expo-linear-gradient";

const horizontalScroll = (props) => {
  const [data, setData] = useState([]);
  const [title, setTitle] = useState([]);
  const [loading, setLoading] = useState(false);
  const finalUrl = props.url;

  useEffect(() => {
    const fetchData = async () => {
      try {
        let response = await fetch(`${finalUrl}`);

        let json = await response.json();

        let filteredItems = json.articles.filter((x) => x.urlToImage != null);

        setData(filteredItems);
        setLoading(true);
        setTitle(json.articles[0].title);
      } catch (error) {
        console.error(error);
      }
    };
    fetchData();
  }, []);

  return (
    <View>
      {loading ? (
        <FlatList
          data={data}
          keyExtractor={({ id }, index) => id}
          horizontal
          showsHorizontalScrollIndicator={false}
          onEndReachedThreshold={0.5}
          renderItem={({ item }) => (
            <TouchableOpacity
              style={styles.card}
              onPress={() => {
                Linking.openURL(item.url);
              }}
            >
              <ImageBackground
                source={{ uri: item.urlToImage }}
                style={styles.image}
              >
                <LinearGradient
                  colors={["transparent", "#000"]}
                  style={styles.gradient}
                >
                  <Text
                    numberOfLines={3}
                    ellipsizeMode="tail"
                    style={styles.title}
                  >
                    {item.title}
                  </Text>

                  <Text style={styles.date}>
                    {moment(item.publishedAt).fromNow()}
                  </Text>
                </LinearGradient>
              </ImageBackground>
            </TouchableOpacity>
          )}
        />
      ) : (
        <ActivityIndicator size="large" color="#672" />
      )}
    </View>
  );
};

export default horizontalScroll;

const styles = StyleSheet.create({
  card: {
    height: 250,
    width: 300,
    margin: 5,
    flex: 1,
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "flex-end",
    borderRadius: 30,
    overflow: "hidden",
  },
  gradient: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "flex-end",
    borderRadius: 12,
    overflow: "hidden",
    padding: 10,
  },
  title: {
    color: "#fff",
    fontWeight: "bold",
    fontSize: 16,
    textShadowColor: "black",
    textShadowOffset: { width: -1, height: 0 },
    textShadowRadius: 10,
  },
  date: {
    color: "#fff",
    fontSize: 12,
    textShadowColor: "black",
    textShadowOffset: { width: -1, height: 0 },
    textShadowRadius: 10,
  },
});
