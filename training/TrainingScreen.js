import * as React from "react";
import {
  StyleSheet,
  Text,
  SafeAreaView,
  View,
  TouchableOpacity,
  Platform,
  StatusBar,
  ScrollView,
  Image,
  useWindowDimensions,
} from "react-native";

export default TrainingScreen = ({ navigation }) => {
  const screen = useWindowDimensions();

  return (
    <SafeAreaView style={styles.container}>
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.titleBox}>
          <Text style={styles.title}> Smart Boxing </Text>
        </View>
        <View style={styles.bubble}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate({
                name: "Speed",
              });
            }}
            activeOpacity={0.7}
            style={styles.imageTitle}
          >
            <Image
              source={require("./TrainingAssets/Speed.png")} //created by master1305 - www.freepik.com
              style={[
                styles.image,
                { width: screen.width / 1.05, height: screen.height / 4.5 },
              ]}
            />
            <Text style={styles.text}>Speed</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.bubble}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate({
                name: "Endurance",
              });
            }}
            activeOpacity={0.7}
            style={styles.imageTitle}
          >
            <Image
              source={require("./TrainingAssets/Endurance.png")} //created by master1305 - www.freepik.com
              style={[
                styles.image,
                {
                  width: screen.width / 1.05,
                  height: screen.height / 4.5,
                },
              ]}
            />
            <Text style={styles.text}>Endurance</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.bubble}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate({
                name: "Power",
              });
            }}
            activeOpacity={0.7}
            style={styles.imageTitle}
          >
            <Image
              source={require("./TrainingAssets/Power.png")} //created by master1305 - www.freepik.com
              style={[
                styles.image,
                { width: screen.width / 1.05, height: screen.height / 4.5 },
              ]}
            />
            <Text style={styles.text}>Power</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.bubble}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate({
                name: "Agility",
              });
            }}
            activeOpacity={0.7}
            style={styles.imageTitle}
          >
            <Image
              source={require("./TrainingAssets/Agility.png")} //created by serhii_bobyk - www.freepik.com
              style={[
                styles.image,
                { width: screen.width / 1.05, height: screen.height / 4.5 },
              ]}
            />
            <Text style={styles.text}>Agility</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.bubble}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate({
                name: "Technique",
              });
            }}
            activeOpacity={0.7}
            style={styles.imageTitle}
          >
            <Image
              source={require("./TrainingAssets/Technique.png")} //created by master1305 - www.freepik.com
              style={[
                styles.image,
                { width: screen.width / 1.05, height: screen.height / 4.5 },
              ]}
            />
            <Text style={styles.text}>Technique</Text>
          </TouchableOpacity>
        </View>
        <View style={[styles.bubble, { marginBottom: screen.height / 6 }]}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate({
                name: "Abs",
              });
            }}
            activeOpacity={0.7}
            style={styles.imageTitle}
          >
            <Image
              source={require("./TrainingAssets/Abs.png")} //created by master1305 - www.freepik.com
              style={[
                styles.image,
                { width: screen.width / 1.05, height: screen.height / 4.5 },
              ]}
            />
            <Text style={styles.text}>Abs</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};
const styles = StyleSheet.create({
  bubble: {
    marginTop: 40,
    justifyContent: "center",
    alignItems: "center",
  },
  container: {
    flex: 1,
    backgroundColor: "#AAC7FF",
    paddingTop: Platform.OS === "android" ? StatusBar.currentHeight : 0,
  },
  image: {
    borderRadius: 50,
  },
  imageTitle: {
    justifyContent: "center",
    alignItems: "center",
  },
  text: {
    position: "absolute",
    fontFamily: "Marker",
    fontSize: 40,
    color: "#FCFF00",
  },
  titleBox: {
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    borderBottomWidth: 15,
    borderBottomColor: "black",
    justifyContent: "center",
    alignItems: "center",
    paddingBottom: 15,
  },
  title: {
    fontSize: 52,
    color: "black",
    fontFamily: "Marker",
  },
});
